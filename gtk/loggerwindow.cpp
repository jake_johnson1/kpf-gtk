#include "include/loggerwindow.h"

LoggerWindow::LoggerWindow()
{
	set_default_size(511, 386);
	set_title("KPF Logger Console");

	builder = Gtk::Builder::create();
	builder->add_from_resource("/com/kse/ui/loggerwindow.glade");

	builder->get_widget("boxContainer", boxContainer);
	builder->get_widget("bClose", bClose);
	builder->get_widget("tvLog", tvLog);

	buffer = Gtk::TextBuffer::create();

	boxContainer->show_all();

	add(*boxContainer);

	bClose->signal_clicked().connect(sigc::mem_fun(*this,
		&LoggerWindow::on_close_button_clicked));
}

LoggerWindow::~LoggerWindow()
{}

void LoggerWindow::append(Glib::ustring text)
{
	log_content += getDateTime() + ": " + text + "\n";
	buffer->set_text(log_content);
	tvLog->set_buffer(buffer);
}

void LoggerWindow::timestamp(Glib::ustring text)
{
	log_content += text + "\n";
	buffer->set_text(log_content);
	tvLog->set_buffer(buffer);
}

void LoggerWindow::on_close_button_clicked()
{
	hide();
}

Glib::ustring LoggerWindow::getLogContent()
{
	return log_content;
}

const std::string LoggerWindow::getDateTime()
{
	auto start = std::chrono::system_clock::now();
	time_t time = std::chrono::system_clock::to_time_t(start);
	std::string time_string = ctime(&time);
	time_string.erase(std::remove(time_string.begin(), time_string.end(), '\n'), time_string.end());

	return time_string;
}