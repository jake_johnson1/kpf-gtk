#ifndef ABOUT_H
#define ABOUT_H

#include <gtkmm/aboutdialog.h>
#include <gtkmm/window.h>
#include <gtkmm/image.h>
#include <gdkmm/pixbuf.h>

#include <vector>
#include <iostream>

#include "include/refs.h"

class About : public Gtk::AboutDialog
{
public:
	About(Gtk::Window* parent);
	virtual ~About();

protected:
	void on_about_dialog_response(int response_id);
};

#endif // ABOUT_H