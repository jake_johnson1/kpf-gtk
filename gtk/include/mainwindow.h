#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/messagedialog.h>

#include <glib.h>

#include <fstream>
#include <string>

#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

#include "loggerwindow.h"
#include "about.h"
#include "refs.h"

class MainWindow : public Gtk::Window
{
public:
    MainWindow();
    virtual ~MainWindow();

protected:
    // slots for signals
    void on_main_quit();
    void on_export_button_clicked();
    void on_browse_button_clicked();
    void on_logger_menu_item_activated();
    void on_about_menu_item_activated();
    void on_open_logs_dir_menu_item_activated();
    void on_logger_window_closed();
    void on_about_dialog_closed();

private:
    void buildWindow();
    const char* getHomeDir();
    void log(Glib::ustring);

    LoggerWindow* logger = nullptr;
    About* about = nullptr;

    Glib::RefPtr<Gtk::Builder> builder;
    Gtk::Box* mainContainer;
    
    // buttons
    Gtk::Button* bBrowseK2;
    Gtk::Button* bQuit;
    Gtk::Button* bExport;

    // menu items
    Gtk::MenuItem* miQuit;
    Gtk::MenuItem* miLogger;
    Gtk::MenuItem* miAbout;
    Gtk::MenuItem* miOpenLogsDir;

    // entries
    Gtk::Entry* tbKotor2;
};

#endif // MAINWINDOW_H