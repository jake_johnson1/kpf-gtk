#include "include/about.h"

About::About(Gtk::Window* parent)
{
	set_transient_for(*parent);

	auto image = new Gtk::Image;
	image->set_from_resource("/com/kse/data/icons/app/fett.png");
	auto pix = image->get_pixbuf();

	std::string version = std::to_string(VERSION_MAJOR)
		+ "." + std::to_string(VERSION_MINOR);

	set_logo(pix);
	set_program_name("KPF: KSE PathFinder");
	set_version(version);
	set_copyright("Copyright © 2016 KSE Team");
	set_comments("KPF: KSE PathFinder is a simple tool used to grab the paths \
for Star Wars: Knights of the Old Republic 1 and 2, and used to generate \
configuration files for KSE.");
	set_license("KPF is free software; you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as\n\
published by the Free Software Foundation; either version 2 of the\n\
License, or (at your option) any later version.\n\
\n\
KPF is distributed in the hope that it will be useful\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with KPF; if not, write to the Free Software\n\
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,\n\
MA 02110-1301, USA.");

	std::vector<Glib::ustring> authors = {
		"Kaleb Klein <klein.jae@gmail.com>"
	};
	set_authors(authors);

	std::vector<Glib::ustring> documenters = {
		"Kaleb Klein <klein.jae@gmail.com>"
	};
	set_documenters(documenters);

	set_website("https://bitbucket.org/kotorsge-team/kpf-gtk");
	set_website_label("Visit the kpf-gtk BitBucket Repository");

	signal_response().connect(sigc::mem_fun(*this,
		&About::on_about_dialog_response));
}

About::~About()
{}

void About::on_about_dialog_response(int response_id)
{
	if(response_id == Gtk::RESPONSE_DELETE_EVENT)
	{
		hide();
	}
}