#!/bin/bash
# Autogen.sh will run the autoconf and automake tools
# to configure and build kpf-gtk

if test -z `which aclocal`; then
	echo "aclocal not found on system!"
	exit 1
fi

if test -z `which autoconf`; then
	echo "autoconf not found on system!"
	exit 1
fi

if test -z `which automake`; then
	echo "automake not found on system!"
	exit 1
fi

if test -z `which make`; then
	echo "make not found on system!"
	exit 1
fi

for i in "$@"
do
	case $i in
		-c|--clean)
			CMD=clean
			shift
			;;
	esac
done

if [[ ${CMD} && ${CMD-x} ]]; then
	echo "Cleaning up"
	if [ -e build ]; then rm -rf build; fi
	if [ -e autom4te.cache ]; then rm -rf autom4te.cache; fi
	if [ -e aclocal.m4 ]; then rm aclocal.m4; fi
	if [ -e compile ]; then rm compile; fi
	if [ -e configure ]; then rm configure; fi
	if [ -e depcomp ]; then rm depcomp; fi
	if [ -e install-sh ]; then rm install-sh; fi
	if [ -e Makefile.in ]; then rm Makefile.in; fi
	if [ -e missing ]; then rm missing; fi
	cd gtk
	if [ -e Makefile.in ]; then rm Makefile.in; fi
	cd data
	if [ -e Makefile.in ]; then rm Makefile.in; fi
	unset CMD
else
	# Run automake stuff
	if [ -e "configure" ]; then
		autoreconf
		cd build
		../configure
	else
		aclocal
		autoconf
		automake --add-missing

		mkdir build
		cd build
		../configure
	fi

	echo "Environment built. Run make inside the new build directory to build kpf-gtk"
fi
